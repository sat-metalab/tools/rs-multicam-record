// License: Apache 2.0. See LICENSE file in root directory.
// Copyright(c) 2019 Intel Corporation. All Rights Reserved.

#include <chrono>
#include <functional>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <stdio.h>
#include <string.h>
#include <thread>

#include <librealsense2/rs.hpp>
#include <tclap/CmdLine.h>

using namespace TCLAP;

int main(int argc, char* argv[])
try
{
    // Parse command line arguments
    CmdLine cmd("librealsense rs-multicam-record example tool", ' ');
    ValueArg<int> time("t", "time", "stop recording after specified duration (in seconds, -1 for infinite)", false, -1, "-1");
    SwitchArg reset("r", "reset", "hardware reset devices before recording", false);
    SwitchArg color("c", "color", "Activate capture for RGB camera", false);
    SwitchArg infrared("i", "infrared", "Activate capture for infrared camera", false);
    SwitchArg depth("d", "depth", "Activate capture for depth", false);
    SwitchArg pose("p", "pose", "Activate capture for pose (if available)", false);
    ValueArg<std::string> out_folder("f", "full-folder-path", "the folder path where the data will be saved to", false, ".", "./");
    ValueArg<std::string> rgb_size("", "rgb-size", "RGB capture size", false, "848x480", "848x480");
    ValueArg<std::string> ir_size("", "ir-size", "IR capture size", false, "848x480", "848x480");
    ValueArg<int32_t> framerate("", "fps", "Framerate for all inputs", false, 30, "FPS");

    cmd.add(time);
    cmd.add(reset);
    cmd.add(color);
    cmd.add(infrared);
    cmd.add(depth);
    cmd.add(pose);
    cmd.add(out_folder);
    cmd.add(rgb_size);
    cmd.add(ir_size);
    cmd.add(framerate);
    cmd.parse(argc, argv);

    // Get RGB stream size
    int rgb_width, rgb_height;
    {
        bool success = false;
        const auto separator_pos = rgb_size.getValue().find("x");
        if (separator_pos != std::string::npos)
        {
            try
            {
                rgb_width = std::stoi(rgb_size.getValue().substr(0, separator_pos));
                rgb_height = std::stoi(rgb_size.getValue().substr(separator_pos + 1));
                success = true;
            }
            catch (...)
            {
                success = false;
            }
        }

        if (!success)
        {
            std::cerr << "RGB stream size must be of the form WIDTHxHEIGHT\n";
            return EXIT_FAILURE;
        }
    }

    // Get IR stream size
    int ir_width, ir_height;
    {
        bool success = false;
        const auto separator_pos = ir_size.getValue().find("x");
        if (separator_pos != std::string::npos)
        {
            try
            {
                ir_width = std::stoi(ir_size.getValue().substr(0, separator_pos));
                ir_height = std::stoi(ir_size.getValue().substr(separator_pos + 1));
                success = true;
            }
            catch (...)
            {
                success = false;
            }
        }

        if (!success)
        {
            std::cerr << "IR stream size must be of the form WIDTHxHEIGHT\n";
            return EXIT_FAILURE;
        }
    }

    if (!color.getValue() && !infrared.getValue() && !depth.getValue())
    {
        std::cerr << "No data stream activated, activate color, infrared or depth stream for recording\n";
        return EXIT_FAILURE;
    }

    std::cout << "Streams activated for recording: ";
    if (color.getValue())
        std::cout << "color ";
    if (infrared.getValue())
        std::cout << "infrared ";
    if (depth.getValue())
        std::cout << "depth";
    if (pose.getValue())
        std::cout << "pose";
    std::cout << "\n";

    // Create librealsense context for managing devices
    rs2::context ctx;

    std::vector<rs2::pipeline> pipelines;

    // Capture serial numbers before opening streaming
    std::vector<std::string> serials;
    for (auto&& dev : ctx.query_devices())
    {
        serials.push_back(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
        if (reset.getValue())
        {
            std::cout << "Hardware resetting device with serial number " << serials.back() << std::endl;
            dev.hardware_reset();
        }
    }

    // Get current date
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d-%H-%M-%S");
    std::cout << ss.str() << std::endl;

    // Start a recording pipeline for each connected device
    for (auto&& serial : serials)
    {
        rs2::pipeline pipe(ctx);
        rs2::config cfg;

        std::stringstream filename;
        filename << out_folder.getValue() << "/" << ss.str() << "-" << serial << ".bag";
        std::cout << "Enable record to file " << filename.str() << std::endl;
        cfg.enable_device(serial);
        if (color.getValue())
            cfg.enable_stream(RS2_STREAM_COLOR, rgb_width, rgb_height, RS2_FORMAT_YUYV, framerate.getValue());
        if (infrared.getValue())
            cfg.enable_stream(RS2_STREAM_INFRARED, ir_width, ir_height, RS2_FORMAT_Y8, framerate.getValue());
        if (depth.getValue())
            cfg.enable_stream(RS2_STREAM_DEPTH, ir_width, ir_height, RS2_FORMAT_Z16, framerate.getValue());
        if (pose.getValue())
        {
            cfg.enable_stream(RS2_STREAM_GYRO);
            cfg.enable_stream(RS2_STREAM_ACCEL);
        }
        cfg.enable_record_to_file(filename.str());
        pipe.start(cfg);
        pipelines.emplace_back(pipe);
    }

    auto t = std::chrono::system_clock::now();
    auto t0 = t;
    auto duration = time.getValue();

    if (duration != -1)
    {
        auto tk = t;
        std::cout << "Recording left: " << duration << "s";
        while (t - t0 <= std::chrono::seconds(time.getValue()))
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            t = std::chrono::system_clock::now();
            if (t - tk >= std::chrono::seconds(1))
            {
                std::cout << "\r" << std::setprecision(3) << std::fixed << "Recording left: " << duration - std::chrono::duration_cast<std::chrono::seconds>(t - t0).count() << "s"
                          << std::flush;
                tk = t;
            }
        }
        std::cout << "\r"
                  << "Recording done, duration: " << duration << "s." << std::endl;
    }
    else
    {
        std::cout << "Press Enter to end recording(s): ";
        while (std::cin.get() != '\n')
            ;
    }

    for (auto&& pipeline : pipelines)
    {
        pipeline.stop();
    }

    return EXIT_SUCCESS;
}
catch (const rs2::error& e)
{
    std::cerr << "RealSense error calling " << e.get_failed_function() << "(" << e.get_failed_args() << "):\n    " << e.what() << std::endl;
    return EXIT_FAILURE;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
